#!/usr/bin/ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'pp'

begin
  doc = Nokogiri::HTML(open("http://www.emc.com/security/rsa-securid/rsa-securid-software-authenticators/mac-os.htm",
	  "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 1074) AppleWebKit/536.25 (KHTML like Gecko) Version/6.0 Safari/536.25") )
rescue
  puts "Failed to fetch download page"
  exit 1
end

link = doc.xpath("//a").find do |link|
  link.text.match(/Download Application/) && link['href'].match(/.*dmg/)
end

puts link['href']

