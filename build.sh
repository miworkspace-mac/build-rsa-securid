#!/bin/bash -ex

# CONFIG
prefix="RSASecurID"
suffix=""
package_name="RSASecurID"
icon_name=""
category="Utilities"
description='The RSA SecurID Application is used to store your software MToken on your device. You must install the RSA SecurID application on your device before requesting a software MToken. Information on how to activate your software MToken can be found here: <a href="http://www.itcs.umich.edu/itcsdocs/s4394/#activatesoftware">http://www.itcs.umich.edu/itcsdocs/s4394/#activatesoftware</a>'
url=`./finder.rb`

# download it
curl -o app.dmg $url

# Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

# expand pkg
package_on_mount=`ls ${mountpoint}/RSA*pkg`
/usr/sbin/pkgutil --expand "${package_on_mount}" expanded

# cleanup
hdiutil detach "${mountpoint}"

# unpack payload
mkdir build-root
payload=`(find expanded -name Payload)`
(cd build-root; pax -rz -f "../${payload}")

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 5 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg "${key_files}" | /bin/bash > app.plist

# remove build-root
perl -p -i -e 's/build-root\/pkg//' app.plist

plist=`pwd`/app.plist
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${package_name}"
defaults write "${plist}" display_name "RSA SecurID Token"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
